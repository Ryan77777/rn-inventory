import {StyleSheet, View} from 'react-native';
import React from 'react';
import {
  Container,
  Header,
  TextSemiBold,
  Gap,
  StatusBar,
  Card,
} from 'components';
import {colors} from 'utils';

const Report = ({navigation}) => {
  return (
    <Container>
      <StatusBar />
      <Header
        type="main"
        title="Laporan"
        onPress={() => navigation.navigate('Home')}
      />
      <View style={styles.container}>
        <Gap height={20} />
        <TextSemiBold type="TextInput" text="Menu" color={colors['Color-6']} />
        <Gap height={24} />
        <Card
          title="Barang Masuk"
          color={colors['Color-1']}
          onPress={() => navigation.navigate('ReportIncome')}
        />
        <Card
          title="Barang Keluar"
          color={colors['Color-1']}
          onPress={() => navigation.navigate('ReportOutcome')}
        />
      </View>
    </Container>
  );
};

export default Report;

const styles = StyleSheet.create({
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    marginBottom: 24,
  },
  container: {
    marginHorizontal: 24,
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 24,
  },
});
