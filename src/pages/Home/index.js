import {StyleSheet, View} from 'react-native';
import React from 'react';
import {ILProduct, ILSale, ILBuy, Box} from 'assets';
import {
  Container,
  TextSemiBold,
  Gap,
  StatusBar,
  Card,
  TextButton,
} from 'components';
import {colors} from 'utils';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ILReport} from '../../assets/Illustrations';

const Home = ({navigation}) => {
  const onLogout = async () => {
    try {
      await auth()
        .signOut()
        .then(() => {
          AsyncStorage.removeItem('token');
          AsyncStorage.removeItem('userProfile');
          navigation.replace('Login');
        });
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <Container>
      <StatusBar />
      <View style={styles.header}>
        <Box />
        <Gap height={8} />
        <TextSemiBold
          type="Body"
          text="Nusantara Makmur"
          color={colors['Color-6']}
        />
      </View>
      <View style={styles.container}>
        <TextSemiBold type="TextInput" text="Menu" color={colors['Color-6']} />
        <Gap height={24} />
        <Card
          title="Produk"
          icon={<ILProduct />}
          onPress={() => navigation.navigate('Product')}
        />
        <Card
          title="Barang Masuk"
          icon={<ILBuy />}
          color={colors['Color-1']}
          onPress={() => navigation.navigate('Income')}
        />
        <Card
          title="Barang Keluar"
          icon={<ILSale />}
          color={colors['Color-9']}
          onPress={() => navigation.navigate('Outcome')}
        />
        <Card
          title="Laporan"
          icon={<ILReport />}
          color={colors['Color-9']}
          onPress={() => navigation.navigate('Report')}
        />
      </View>
      <View style={styles.footer}>
        <TextButton text="Keluar" onPress={onLogout} />
      </View>
    </Container>
  );
};

export default Home;

const styles = StyleSheet.create({
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    marginBottom: 24,
  },
  container: {
    marginHorizontal: 24,
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 24,
  },
});
