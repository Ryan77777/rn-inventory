import React, {useEffect} from 'react';
import {SplashScreen} from 'assets';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Dimensions, Image, StyleSheet, View} from 'react-native';

const Splashscreen = ({navigation}) => {
  useEffect(() => {
    const getToken = async () => {
      const token = await AsyncStorage.getItem('token');
      setTimeout(() => {
        if (token) {
          navigation.replace('Home');
        } else {
          navigation.replace('Login');
        }
      }, 2000);
    };
    getToken();
  }, [navigation]);

  return (
    <View style={StyleSheet.absoluteFillObject}>
      <Image source={SplashScreen} style={styles.img} resizeMode="cover" />
    </View>
  );
};

export default Splashscreen;

const styles = StyleSheet.create({
  img: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
});
