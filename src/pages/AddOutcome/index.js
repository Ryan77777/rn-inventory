import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {
  KeyboardAvoiding,
  StatusBar,
  TextInput,
  FilledButton,
  Container,
  Header,
  Loading,
} from 'components';
import {scaleSize} from 'utils';
import DatePicker from 'react-native-date-picker';
import Moment from 'moment';
import 'moment/locale/id';
import firestore from '@react-native-firebase/firestore';
import {showMessage} from 'react-native-flash-message';

const AddOutcome = ({navigation}) => {
  const [state, setState] = useState({
    product: '',
    total: '',
    date: new Date(),
  });
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState([]);

  const getData = async () => {
    firestore()
      .collection('Products')
      .orderBy('name', 'asc')
      .get()
      .then(querySnapshot => {
        const temp = [];
        querySnapshot.forEach(doc => {
          temp.push({label: doc.data().name, value: doc.id});
        });
        setData(temp);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  const onDisabled = () => {
    const {product, total, date} = state;
    const form = {product, total, date};
    const dataInputEmpty = Object.keys(form).filter(x => form[x] === '');
    const dataEmpty = [...dataInputEmpty];

    if (dataEmpty.length) {
      for (let index = 0; index < dataEmpty.length; index++) {
        return true;
      }
    } else {
      return false;
    }
  };

  const updateStock = async id => {
    const product = await firestore().collection('Products').doc(id).get();
    const stock = product.data().stock;
    const after = parseInt(stock, 10) - parseInt(state.total, 10);
    if (after >= 0) {
      firestore().collection('Products').doc(id).update({
        stock: after,
      });
    }
    return after;
  };

  const onSubmit = async () => {
    setIsLoading(true);
    try {
      setIsLoading(false);
      const x = await updateStock(state.product);
      if (x >= 0) {
        firestore()
          .collection('Outcome')
          .add(state)
          .then(() => {
            navigation.replace('Outcome');
          });
      } else {
        showMessage({
          message: 'Stock tidak tersedia',
          type: 'danger',
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {isLoading && <Loading />}
      <Container>
        <StatusBar />
        <Header title="Barang Keluar" onPress={() => navigation.goBack()} />
        <KeyboardAvoiding>
          <View style={styles.container}>
            <TextInput
              type="select"
              label="Nama Barang"
              placeholder="-- Pilih Barang --"
              data={data}
              onSelect={item =>
                setState({
                  ...state,
                  product: item.value,
                })
              }
            />
            <TextInput
              label="Jumlah"
              placeholder="Masukan jumlah"
              value={state.total}
              onChangeText={value =>
                setState({
                  ...state,
                  total: value,
                })
              }
              keyboardType="number-pad"
              returnKeyType="done"
            />
            <TextInput
              type="disable"
              label="Tanggal Keluar"
              value={Moment(state.date).format('DD-MM-YYYY')}
              onPress={() => setOpen(true)}
            />

            <FilledButton
              text="Simpan"
              disable={onDisabled()}
              onPress={onSubmit}
            />
          </View>
        </KeyboardAvoiding>

        <DatePicker
          modal
          mode="date"
          open={open}
          date={state.date}
          onConfirm={x => {
            setOpen(false);
            setState({
              ...state,
              date: x,
            });
          }}
          onCancel={() => {
            setOpen(false);
          }}
        />
      </Container>
    </>
  );
};

export default AddOutcome;

const styles = StyleSheet.create({
  container: {
    margin: scaleSize(24),
  },
  forgetWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
