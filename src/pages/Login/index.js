import React, {useRef, useState} from 'react';
import {Keyboard, StyleSheet, View} from 'react-native';
import {
  KeyboardAvoiding,
  StatusBar,
  Gap,
  TextSemiBold,
  TextInput,
  FilledButton,
  Divider,
  TextButton,
  Container,
  Loading,
} from 'components';
import {colors, scaleSize} from 'utils';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Login = ({navigation}) => {
  const ref = useRef(null);

  const [state, setState] = useState({
    email: '',
    password: '',

    isErrorEmail: false,
    isErrorPassword: false,

    emailMessage: '',
    passwordMessage: '',
  });

  const [isLoading, setIsLoading] = useState(false);

  const getUser = async id => {
    const user = await firestore()
      .collection('Users')
      .where('uid', '==', id)
      .get();
    AsyncStorage.setItem('userProfile', JSON.stringify(user.docs[0].data()));
  };

  const onSignin = async () => {
    Keyboard.dismiss();
    try {
      setIsLoading(true);
      await auth()
        .signInWithEmailAndPassword(state.email, state.password)
        .then(res => {
          setIsLoading(false);
          getUser(res.user.uid);
          AsyncStorage.setItem('token', res.user.uid);
          navigation.replace('Home');
        })
        .catch(error => {
          setIsLoading(false);
          if (error.code === 'auth/user-not-found') {
            setState({
              ...state,
              isErrorEmail: true,
              emailMessage:
                'There is no user record corresponding to this identifier.',
            });
          }

          if (error.code === 'auth/invalid-email') {
            setState({
              ...state,
              isErrorEmail: true,
              emailMessage: 'That email address is invalid!',
            });
          }

          if (error.code === 'auth/wrong-password') {
            setState({
              ...state,
              isErrorPassword: true,
              passwordMessage:
                'The password is invalid or the user does not have a password.',
            });
          }
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {isLoading && <Loading />}
      <Container>
        <StatusBar />
        <KeyboardAvoiding>
          <View style={styles.container}>
            <TextSemiBold
              type="Body"
              text="Login akun kamu"
              color={colors['Color-6']}
            />
            <Gap height={20} />
            <TextInput
              label="Email"
              placeholder="Masukan email"
              value={state.email}
              onChangeText={value =>
                setState({
                  ...state,
                  email: value,
                })
              }
              keyboardType="default"
              returnKeyType="next"
              onSubmitEditing={() => ref.current.focus()}
              isError={state.isErrorEmail}
              errorMessage={state.emailMessage}
            />
            <TextInput
              ref={ref}
              type="password"
              label="Password"
              placeholder="Masukan password"
              value={state.password}
              onChangeText={value =>
                setState({
                  ...state,
                  password: value,
                })
              }
              isError={state.isErrorPassword}
              errorMessage={state.passwordMessage}
            />
            <FilledButton text="Login" disable={false} onPress={onSignin} />
            <Gap height={12} />
            <Divider />
            <Gap height={12} />
            <TextButton
              text="Daftar sekarang"
              onPress={() => navigation.navigate('Register')}
            />
          </View>
        </KeyboardAvoiding>
      </Container>
    </>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    margin: scaleSize(24),
  },
  forgetWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
