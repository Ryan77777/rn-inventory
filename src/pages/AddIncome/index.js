import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {
  KeyboardAvoiding,
  StatusBar,
  TextInput,
  FilledButton,
  Container,
  Header,
  Loading,
} from 'components';
import {scaleSize} from 'utils';
import DatePicker from 'react-native-date-picker';
import Moment from 'moment';
import 'moment/locale/id';
import firestore from '@react-native-firebase/firestore';

const AddIncome = ({navigation}) => {
  const data = [];

  const [state, setState] = useState({
    product: '',
    total: '',
    date: new Date(),
  });
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const getData = async () => {
    firestore()
      .collection('Products')
      .orderBy('name', 'asc')
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          data.push({label: doc.data().name, value: doc.id});
        });
      });
  };

  useEffect(() => {
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onDisabled = () => {
    const {product, total, date} = state;
    const form = {product, total, date};
    const dataInputEmpty = Object.keys(form).filter(x => form[x] === '');
    const dataEmpty = [...dataInputEmpty];

    if (dataEmpty.length) {
      for (let index = 0; index < dataEmpty.length; index++) {
        return true;
      }
    } else {
      return false;
    }
  };

  const updateStock = async id => {
    const product = await firestore().collection('Products').doc(id).get();
    const stock = product.data().stock;
    const after = parseInt(stock) + parseInt(state.total);
    firestore().collection('Products').doc(id).update({
      stock: after,
    });
  };

  const onSubmit = async () => {
    setIsLoading(true);
    try {
      firestore()
        .collection('Income')
        .add(state)
        .then(() => {
          updateStock(state.product);
          setIsLoading(false);
          navigation.replace('Income');
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {isLoading && <Loading />}
      <Container>
        <StatusBar />
        <Header title="Barang Masuk" onPress={() => navigation.goBack()} />
        <KeyboardAvoiding>
          <View style={styles.container}>
            <TextInput
              type="select"
              label="Nama Barang"
              placeholder="-- Pilih Barang --"
              data={data}
              onSelect={item =>
                setState({
                  ...state,
                  product: item.value,
                })
              }
            />
            <TextInput
              label="Jumlah"
              placeholder="Masukan jumlah"
              value={state.total}
              onChangeText={value =>
                setState({
                  ...state,
                  total: value,
                })
              }
              keyboardType="number-pad"
              returnKeyType="done"
            />
            <TextInput
              type="disable"
              label="Tanggal Masuk"
              value={Moment(state.date).format('DD-MM-YYYY')}
              onPress={() => setOpen(true)}
            />

            <FilledButton
              text="Simpan"
              disable={onDisabled()}
              onPress={onSubmit}
            />
          </View>
        </KeyboardAvoiding>

        <DatePicker
          modal
          mode="date"
          open={open}
          date={state.date}
          onConfirm={x => {
            setOpen(false);
            setState({
              ...state,
              date: x,
            });
          }}
          onCancel={() => {
            setOpen(false);
          }}
        />
      </Container>
    </>
  );
};

export default AddIncome;

const styles = StyleSheet.create({
  container: {
    margin: scaleSize(24),
  },
  forgetWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
