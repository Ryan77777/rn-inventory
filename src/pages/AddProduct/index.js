import React, {useRef, useState} from 'react';
import {Keyboard, StyleSheet, View} from 'react-native';
import {
  KeyboardAvoiding,
  StatusBar,
  TextInput,
  FilledButton,
  Container,
  Header,
  Loading,
} from 'components';
import {scaleSize} from 'utils';
import firestore from '@react-native-firebase/firestore';

const AddProduct = ({navigation}) => {
  const ref = useRef(null);
  const ref1 = useRef(null);
  const ref2 = useRef(null);

  const [state, setState] = useState({
    code: '',
    name: '',
    price: '',
    stock: '',
  });

  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const onDisabled = () => {
    const {code, name, price, stock} = state;
    const form = {code, name, price, stock};
    const dataInputEmpty = Object.keys(form).filter(data => form[data] === '');
    const dataEmpty = [...dataInputEmpty];

    if (dataEmpty.length) {
      for (let index = 0; index < dataEmpty.length; index++) {
        return true;
      }
    } else {
      return false;
    }
  };

  const onSubmit = async () => {
    setIsLoading(true);
    Keyboard.dismiss();
    try {
      await firestore()
        .collection('Products')
        .where('code', '==', state.code)
        .get()
        .then(querySnapshot => {
          if (querySnapshot.docs.length > 0) {
            setIsLoading(false);
            setIsError(true);
          } else {
            firestore()
              .collection('Products')
              .add(state)
              .then(() => {
                setIsLoading(false);
                navigation.navigate('Product');
              });
          }
        });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {isLoading && <Loading />}
      <Container>
        <StatusBar />
        <Header title="Tambah Produk" onPress={() => navigation.goBack()} />
        <KeyboardAvoiding>
          <View style={styles.container}>
            <TextInput
              label="Kode Barang"
              placeholder="Kode barang"
              value={state.code}
              onChangeText={value => {
                setState({
                  ...state,
                  code: value,
                });
                setIsError(false);
              }}
              isError={isError}
              errorMessage="Item code already exists"
              keyboardType="default"
              returnKeyType="next"
              onSubmitEditing={() => ref.current.focus()}
            />
            <TextInput
              ref={ref}
              label="Nama Barang"
              placeholder="Masukan nama barang"
              value={state.name}
              onChangeText={value => {
                setState({
                  ...state,
                  name: value,
                });
                setIsError(false);
              }}
              keyboardType="default"
              returnKeyType="next"
              onSubmitEditing={() => ref1.current.focus()}
            />
            <TextInput
              ref={ref1}
              label="Harga"
              placeholder="Masukan harga"
              value={state.price}
              onChangeText={value => {
                setState({
                  ...state,
                  price: value,
                });
                setIsError(false);
              }}
              keyboardType="number-pad"
              returnKeyType="next"
              onSubmitEditing={() => ref2.current.focus()}
            />
            <TextInput
              ref={ref2}
              label="Stok"
              placeholder="Masukan stok"
              value={state.stock}
              onChangeText={value => {
                setState({
                  ...state,
                  stock: value,
                });
                setIsError(false);
              }}
              keyboardType="number-pad"
              returnKeyType="done"
            />
            <FilledButton
              text="Simpan"
              disable={onDisabled()}
              onPress={onSubmit}
            />
          </View>
        </KeyboardAvoiding>
      </Container>
    </>
  );
};

export default AddProduct;

const styles = StyleSheet.create({
  container: {
    margin: scaleSize(24),
  },
  forgetWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
