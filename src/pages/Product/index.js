import {Dimensions, StyleSheet, View} from 'react-native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  Container,
  StatusBar,
  Header,
  TextInput,
  Gap,
  ListItem,
  FilledButton,
  TextRegular,
} from 'components';
import {colors} from 'utils';
import {ScrollView} from 'react-native-gesture-handler';
import firestore from '@react-native-firebase/firestore';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const RenderSkeleton = () => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        padding={12}
        marginHorizontal={24}
        marginBottom={16}
        borderWidth={1}
        borderColor={colors['Color-5']}
        borderRadius={8}>
        <SkeletonPlaceholder.Item>
          <SkeletonPlaceholder.Item width={120} height={14} borderRadius={4} />
          <SkeletonPlaceholder.Item
            width={80}
            height={14}
            marginTop={6}
            borderRadius={4}
          />
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item width={80} height={14} borderRadius={4} />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

const Product = ({navigation}) => {
  const ref = useRef(null);

  const [keyword, setKeyword] = useState('');

  const [data, setData] = useState();
  const [filter, setFilter] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getData = async () => {
    setIsLoading(true);
    firestore()
      .collection('Products')
      .orderBy('name', 'asc')
      .get()
      .then(querySnapshot => {
        let tempDoc = [];
        querySnapshot.forEach(doc => {
          tempDoc.push({id: doc.id, ...doc.data()});
        });
        setData(tempDoc);
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      });
  };

  const onDelete = useCallback(id => {
    firestore().collection('Products').doc(id).delete();
    getData();
  }, []);

  const onSearch = useCallback(() => {
    const result = data.filter(x =>
      x.name.toLowerCase().includes(keyword.toLowerCase()),
    );

    setIsLoading(true);
    setTimeout(() => {
      setIsLoading(false);
      setFilter(result);
    }, 1000);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyword]);

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (keyword) {
      onSearch();
    } else {
      getData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [keyword]);

  return (
    <Container backgroundColor={colors['Color-3']}>
      <StatusBar />
      <Header title="Produk" onPress={() => navigation.navigate('Home')} />
      <View style={styles.container}>
        <TextInput
          type="search"
          placeholder="Cari Barang"
          value={keyword}
          onChangeText={value => setKeyword(value)}
          keyboardType="default"
          returnKeyType="search"
        />
      </View>
      <Gap height={24} />
      <ScrollView
        ref={ref}
        showsVerticalScrollIndicator={false}
        nestedScrollEnabled>
        {keyword ? (
          filter?.length > 0 ? (
            filter?.map((item, i) => {
              return isLoading ? (
                <RenderSkeleton key={i} />
              ) : (
                <ListItem
                  key={i}
                  type="product"
                  data={item}
                  onDelete={() => onDelete(item.id)}
                  simultaneousHandlers={ref}
                />
              );
            })
          ) : (
            <View style={styles.emptyContainer}>
              <TextRegular
                type="Body"
                text="Data tidak ditemukan"
                color={colors['Color-6']}
              />
            </View>
          )
        ) : data?.length > 0 ? (
          data?.map((item, i) => {
            return isLoading ? (
              <RenderSkeleton key={i} />
            ) : (
              <ListItem
                key={i}
                type="product"
                data={item}
                onDelete={() => onDelete(item.id)}
                simultaneousHandlers={ref}
              />
            );
          })
        ) : (
          <View style={styles.emptyContainer}>
            <TextRegular
              type="Body"
              text="Belum ada Data Barang"
              color={colors['Color-6']}
            />
          </View>
        )}
      </ScrollView>
      <View style={styles.footer}>
        <FilledButton
          text="Tambah Produk"
          onPress={() => navigation.navigate('AddProduct')}
        />
      </View>
    </Container>
  );
};

export default Product;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 24,
    marginTop: 24,
  },
  footer: {
    padding: 12,
    elevation: 3,
  },
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: (Dimensions.get('window').height * 35) / 100,
  },
});
