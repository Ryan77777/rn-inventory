import React, {useRef, useState} from 'react';
import {Keyboard, ScrollView, StyleSheet, View} from 'react-native';
import {
  KeyboardAvoiding,
  StatusBar,
  Gap,
  TextSemiBold,
  TextInput,
  FilledButton,
  Divider,
  TextButton,
  Container,
  Loading,
} from 'components';
import {colors, scaleSize} from 'utils';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Register = ({navigation}) => {
  const ref = useRef(null);
  const ref1 = useRef(null);
  const ref2 = useRef(null);
  const ref3 = useRef(null);

  const [state, setState] = useState({
    name: '',
    email: '',
    password: '',
    confirm: '',
    department: '',

    isErrorEmail: false,
    isErrorPassword: false,

    emailMessage: '',
    passwordMessage: '',
  });

  const [isLoading, setIsLoading] = useState(false);

  const onRegister = async () => {
    Keyboard.dismiss();
    try {
      setIsLoading(true);
      if (state.confirm === state.password) {
        await auth()
          .createUserWithEmailAndPassword(state.email, state.password)
          .then(res => {
            const data = {
              uid: res.user.uid,
              name: state.name,
              email: state.email,
              department: state.department,
            };

            firestore()
              .collection('Users')
              .add(data)
              .then(querySnapshot => {
                setIsLoading(false);
                AsyncStorage.setItem('token', querySnapshot.id);
                AsyncStorage.setItem('userProfile', JSON.stringify(data));
                navigation.replace('Home');
              });
          })
          .catch(error => {
            setIsLoading(false);
            if (error.code === 'auth/email-already-in-use') {
              setState({
                ...state,
                emailMessage: 'That email address is already in use!',
                isErrorEmail: true,
              });
            }

            if (error.code === 'auth/invalid-email') {
              setState({
                ...state,
                emailMessage: 'That email address is invalid!',
                isErrorEmail: true,
              });
            }

            if (error.code === 'auth/weak-password') {
              setState({
                ...state,
                passwordMessage: 'Password should be at least 6 characters',
                isErrorPassword: true,
              });
            }
          });
      } else {
        setState({
          ...state,
          isError: true,
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {isLoading && <Loading />}
      <Container>
        <StatusBar />
        <ScrollView showsVerticalScrollIndicator={false}>
          <KeyboardAvoiding>
            <View style={styles.container}>
              <TextSemiBold
                type="Body"
                text="Daftar gratis"
                color={colors['Color-6']}
              />
              <Gap height={20} />
              <TextInput
                label="Nama"
                placeholder="Masukan nama"
                value={state.name}
                onChangeText={value =>
                  setState({
                    ...state,
                    name: value,
                    isErrorEmail: false,
                    isErrorPassword: false,
                  })
                }
                keyboardType="default"
                returnKeyType="next"
                onSubmitEditing={() => ref.current.focus()}
              />
              <TextInput
                ref={ref}
                label="Email"
                placeholder="Masukan email"
                value={state.email}
                onChangeText={value =>
                  setState({
                    ...state,
                    email: value,
                    isErrorEmail: false,
                    isErrorPassword: false,
                  })
                }
                keyboardType="default"
                returnKeyType="next"
                isError={state.isErrorEmail}
                errorMessage={state.emailMessage}
                onSubmitEditing={() => ref1.current.focus()}
              />
              <TextInput
                ref={ref1}
                type="password"
                label="Password"
                placeholder="Masukan password"
                value={state.password}
                onChangeText={value =>
                  setState({
                    ...state,
                    password: value,
                    isErrorEmail: false,
                    isErrorPassword: false,
                  })
                }
                returnKeyType="next"
                isError={state.isErrorPassword}
                errorMessage={state.passwordMessage}
                onSubmitEditing={() => ref2.current.focus()}
              />
              <TextInput
                ref={ref2}
                type="password"
                label="Konfirmasi Password"
                placeholder="Masukan konfirmasi password"
                value={state.confirm}
                onChangeText={value =>
                  setState({
                    ...state,
                    confirm: value,
                    isErrorEmail: false,
                    isErrorPassword: false,
                  })
                }
                returnKeyType="next"
                isError={state.isErrorPassword}
                errorMessage="Password and confirm password do not match"
              />
              <TextInput
                ref={ref3}
                label="Divisi"
                placeholder="Masukan divisi"
                value={state.department}
                onChangeText={value =>
                  setState({
                    ...state,
                    department: value,
                    isErrorEmail: false,
                    isErrorPassword: false,
                  })
                }
                keyboardType="default"
                returnKeyType="done"
              />
              <FilledButton
                text="Daftar"
                disable={false}
                onPress={onRegister}
              />
              <Gap height={12} />
              <Divider />
              <Gap height={12} />
              <TextButton
                text="Login"
                onPress={() => navigation.navigate('Login')}
              />
            </View>
          </KeyboardAvoiding>
        </ScrollView>
      </Container>
    </>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    margin: scaleSize(24),
  },
  forgetWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
