import {
  Container,
  Gap,
  Header,
  Loading,
  StatusBar,
  TextInput,
  ListItem,
  TextRegular,
} from 'components';
import Moment from 'moment';
import 'moment/locale/id';
import React, {useCallback, useEffect, useState} from 'react';
import {
  Alert,
  Dimensions,
  PermissionsAndroid,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import DatePicker from 'react-native-date-picker';
import {scaleSize, colors, NumberFormatter} from 'utils';
import {IcSearchLight} from 'assets';
import firestore from '@react-native-firebase/firestore';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import RNHTMLtoPDF from 'react-native-html-to-pdf';

const RenderSkeleton = () => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        padding={12}
        marginHorizontal={24}
        marginBottom={16}
        borderWidth={1}
        borderColor={colors['Color-5']}
        borderRadius={8}>
        <SkeletonPlaceholder.Item>
          <SkeletonPlaceholder.Item width={120} height={14} borderRadius={4} />
          <SkeletonPlaceholder.Item
            width={80}
            height={14}
            marginTop={6}
            borderRadius={4}
          />
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item width={80} height={14} borderRadius={4} />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

const ReportIncome = ({navigation}) => {
  const [state, setState] = useState({
    product: '',
    total: '',
    dateFrom: new Date(),
    dateTo: new Date(),
  });

  const [data, setData] = useState([]);

  const [open, setOpen] = useState(false);
  const [isShow, setIsShow] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isFirst, setIsFirst] = useState(true);

  const onSubmit = useCallback(async () => {
    setIsLoading(true);
    try {
      firestore()
        .collection('Income')
        .where('date', '>=', state.dateFrom)
        .where('date', '<=', state.dateTo)
        .get()
        .then(querySnapshot => {
          let tempDoc = [];
          querySnapshot.forEach(async doc => {
            const x = await firestore()
              .collection('Products')
              .doc(doc?.data()?.product)
              .get();
            tempDoc.push({id: doc.id, ...doc.data(), ...x.data()});
          });
          setData(tempDoc);
        });
    } catch (error) {
      console.log(error);
    }
  }, [state]);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
      setIsFirst(false);
    }, 3000);
  }, [data]);

  const htmlContent = `
        <html>
          <head>
            <meta charset="utf-8">
            <title>Invoice</title>
            <link rel="license" href="https://www.opensource.org/licenses/mit-license/">
            <style>
              ${htmlStyles}
            </style>
          </head>
          <body>
            <header>
              <h1>Laporan Barang Masuk</h1>
            </header>
            <table class="inventory">
              <thead>
                <tr>
                  <th><span>No.</span></th>
                  <th><span>Kode Barang</span></th>
                  <th><span>Nama Barang</span></th>
                  <th><span>Harga</span></th>
                  <th><span>Jumlah</span></th>
                  <th><span>Tanggal</span></th>
                </tr>
              </thead>
              <tbody>
              ${data
                .map(
                  (item, index) =>
                    `<tr>
                    <td>
                      <span>${index + 1}</span>
                    </td>
                    <td>
                      <span>${item?.code}</span>
                    </td>
                    <td>
                      <span>${item?.name}</span>
                    </td>
                    <td>
                      <span>${NumberFormatter(item?.price, 'Rp. ')}</span>
                    </td>
                    <td>
                      <span>${item?.total}</span>
                    </td>
                    <td>
                      <span>${Moment(item?.date?.toDate()).format(
                        'DD-MM-YYYY',
                      )}</span>
                    </td>
                  </tr>`,
                )
                .join('')}
              </tbody>
            </table>
          </body>
        </html>
      `;

  const askPermission = () => {
    async function requestExternalWritePermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'Pdf creator needs External Storage Write Permission',
            message: 'Pdf creator needs access to Storage data in your SD Card',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          createPDF();
        }
      } catch (err) {
        console.warn(err);
      }
    }
    if (Platform.OS === 'android') {
      requestExternalWritePermission();
    } else {
      createPDF();
    }
  };

  const createPDF = async () => {
    let options = {
      html: htmlContent,
      fileName: `laporan-barang-masuk-${state.dateFrom}-${state.dateTo}`,
      directory: 'Downloads',
    };

    await RNHTMLtoPDF.convert(options);
    Alert.alert('Successfully Exported');
  };

  return (
    <>
      {isLoading && <Loading />}
      <Container backgroundColor={colors['Color-3']}>
        <StatusBar />
        <Header
          title="Laporan Barang Masuk"
          onPress={() => navigation.goBack()}
        />
        <View style={styles.flexOne}>
          {data.length > 0 ? (
            <TouchableHighlight
              style={styles.downloadButton}
              onPress={askPermission}>
              <TextRegular type="Body" text="Print" color={colors['Color-3']} />
            </TouchableHighlight>
          ) : null}
          <View style={styles.container}>
            <View style={styles.flexOne}>
              <TextInput
                type="disable"
                label="Dari"
                value={Moment(state.dateFrom).format('DD-MM-YYYY')}
                onPress={() => setOpen(true)}
              />
            </View>
            <Gap width={16} />
            <View style={styles.flexOne}>
              <TextInput
                type="disable"
                label="Sampai"
                value={Moment(state.dateTo).format('DD-MM-YYYY')}
                onPress={() => setIsShow(true)}
              />
            </View>
            <Gap width={16} />
            <TouchableOpacity
              style={styles.button}
              activeOpacity={0.7}
              onPress={onSubmit}>
              <IcSearchLight />
            </TouchableOpacity>
          </View>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.content}
            nestedScrollEnabled>
            {data?.length > 0 ? (
              data?.map((item, i) => {
                return isLoading ? (
                  <RenderSkeleton key={i} />
                ) : (
                  <ListItem key={i} type="list" data={item} />
                );
              })
            ) : isFirst ? (
              <View style={styles.emptyContainer}>
                <TextRegular
                  type="Body"
                  text="Silahkan filter terlebih dahulu."
                  color={colors['Color-6']}
                />
              </View>
            ) : (
              <View style={styles.emptyContainer}>
                <TextRegular
                  type="Body"
                  text="Data tidak ditemukan."
                  color={colors['Color-6']}
                />
              </View>
            )}
          </ScrollView>
        </View>

        <DatePicker
          modal
          mode="date"
          open={open}
          date={state.dateFrom}
          onConfirm={x => {
            setOpen(false);
            setState({
              ...state,
              dateFrom: x,
            });
          }}
          onCancel={() => {
            setOpen(false);
          }}
        />
        <DatePicker
          modal
          mode="date"
          open={isShow}
          date={state.dateTo}
          onConfirm={x => {
            setIsShow(false);
            setState({
              ...state,
              dateTo: x,
            });
          }}
          onCancel={() => {
            setIsShow(false);
          }}
        />
      </Container>
    </>
  );
};

export default ReportIncome;

const styles = StyleSheet.create({
  container: {
    marginTop: scaleSize(24),
    marginHorizontal: scaleSize(24),
    flexDirection: 'row',
    alignItems: 'center',
  },
  content: {
    flexGrow: 1,
    marginHorizontal: scaleSize(24),
    marginBottom: scaleSize(80),
  },
  flexOne: {
    flex: 1,
  },
  button: {
    backgroundColor: colors['Color-7'],
    height: 46,
    width: 46,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    marginBottom: -4,
  },
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: (Dimensions.get('window').height * 35) / 100,
  },
  downloadButton: {
    backgroundColor: colors['Color-7'],
    height: 46,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 46 / 2,
    position: 'absolute',
    left: 24,
    bottom: 24,
    right: 24,
    zIndex: 1,
  },
});

const htmlStyles = `
*{
  border: 0;
  box-sizing: content-box;
  color: inherit;
  font-family: inherit;
  font-size: inherit;
  font-style: inherit;
  font-weight: inherit;
  line-height: inherit;
  list-style: none;
  margin: 0;
  padding: 0;
  text-decoration: none;
  vertical-align: top;
}
h1 { font: bold 100% sans-serif; letter-spacing: 0.5em; text-align: center; text-transform: uppercase; }
/* table */
table { font-size: 75%; table-layout: fixed; width: 100%; }
table { border-collapse: separate; border-spacing: 2px; }
th, td { border-width: 1px; padding: 0.5em; position: relative; text-align: left; }
th, td { border-radius: 0.25em; border-style: solid; }
th { background: #EEE; border-color: #BBB; }
td { border-color: #DDD; }
/* page */
html { font: 16px/1 'Open Sans', sans-serif; overflow: auto; }
html { background: #999; cursor: default; }
body { box-sizing: border-box;margin: 0 auto; overflow: hidden; padding: 0.25in; }
body { background: #FFF; border-radius: 1px; box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5); }
/* header */
header { margin: 0 0 3em; }
header:after { clear: both; content: ""; display: table; }
header h1 { background: #0652DD; border-radius: 0.25em; color: #FFF; margin: 0 0 1em; padding: 0.5em 0; }
header address { float: left; font-size: 75%; font-style: normal; line-height: 1.25; margin: 0 1em 1em 0; }
header address p { margin: 0 0 0.25em; }
header span, header img { display: block; float: right; }
header span { margin: 0 0 1em 1em; max-height: 25%; max-width: 60%; position: relative; }
header img { max-height: 100%; max-width: 100%; }
/* table items */
table.inventory { clear: both; width: 100%; }
table.inventory th { font-weight: bold; text-align: center; }
table.inventory th:nth-child(1) { width: 5%; }
table.inventory td:nth-child(1) { width: 12%; }
table.inventory td:nth-child(2) { width: 12%; }
table.inventory td:nth-child(3) { width: 38%; }
table.inventory td:nth-child(4) { width: 12%; }
table.inventory td:nth-child(5) { width: 12%; }
table.inventory td:nth-child(6) { width: 12%; }
`;
