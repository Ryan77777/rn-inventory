import Splashscreen from './Splashscreen';
import Login from './Login';
import Register from './Register';
import Home from './Home';
import Product from './Product';
import AddProduct from './AddProduct';
import Income from './Income';
import AddIncome from './AddIncome';
import Outcome from './Outcome';
import AddOutcome from './AddOutcome';
import Report from './Report';
import ReportIncome from './ReportIncome';
import ReportOutcome from './ReportOutcome';

export {
  Splashscreen,
  Login,
  Register,
  Home,
  Product,
  AddProduct,
  Income,
  AddIncome,
  Outcome,
  AddOutcome,
  Report,
  ReportIncome,
  ReportOutcome,
};
