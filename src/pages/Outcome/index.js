import {Dimensions, StyleSheet, View} from 'react-native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  Container,
  StatusBar,
  Header,
  Gap,
  ListItem,
  FilledButton,
  TextRegular,
} from 'components';
import {colors} from 'utils';
import {ScrollView} from 'react-native-gesture-handler';
import firestore from '@react-native-firebase/firestore';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const RenderSkeleton = () => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        padding={12}
        marginHorizontal={24}
        marginBottom={16}
        borderWidth={1}
        borderColor={colors['Color-5']}
        borderRadius={8}>
        <SkeletonPlaceholder.Item>
          <SkeletonPlaceholder.Item width={120} height={14} borderRadius={4} />
          <SkeletonPlaceholder.Item
            width={80}
            height={14}
            marginTop={6}
            borderRadius={4}
          />
        </SkeletonPlaceholder.Item>
        <SkeletonPlaceholder.Item width={80} height={14} borderRadius={4} />
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

const Outcome = ({navigation}) => {
  const ref = useRef(null);

  const [data, setData] = useState();
  const [isLoading, setIsLoading] = useState(true);

  const getData = async () => {
    setIsLoading(true);
    firestore()
      .collection('Outcome')
      .orderBy('date', 'asc')
      .get()
      .then(querySnapshot => {
        let tempDoc = [];
        querySnapshot.forEach(doc => {
          tempDoc.push({id: doc.id, ...doc.data()});
        });
        setData(tempDoc);
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      });
  };

  const onDelete = useCallback(id => {
    firestore().collection('Outcome').doc(id).delete();
    getData();
  }, []);

  useEffect(() => {
    getData();
  }, []);

  return (
    <Container backgroundColor={colors['Color-3']}>
      <StatusBar />
      <Header
        title="Barang Keluar"
        onPress={() => navigation.navigate('Home')}
      />

      <Gap height={24} />
      <ScrollView
        ref={ref}
        showsVerticalScrollIndicator={false}
        nestedScrollEnabled>
        {data?.length > 0 ? (
          data?.map((item, i) => {
            return isLoading ? (
              <RenderSkeleton key={i} />
            ) : (
              <ListItem
                key={i}
                data={item}
                onDelete={() => onDelete(item.id)}
                simultaneousHandlers={ref}
              />
            );
          })
        ) : (
          <View style={styles.emptyContainer}>
            <TextRegular
              type="Body"
              text="Belum ada Data Barang"
              color={colors['Color-6']}
            />
          </View>
        )}
      </ScrollView>
      <View style={styles.footer}>
        <FilledButton
          text="Tambah"
          onPress={() => navigation.navigate('AddOutcome')}
        />
      </View>
    </Container>
  );
};

export default Outcome;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 24,
    marginTop: 24,
  },
  footer: {
    padding: 12,
    elevation: 3,
  },
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: (Dimensions.get('window').height * 35) / 100,
  },
});
