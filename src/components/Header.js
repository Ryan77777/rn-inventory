import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {IcArrowLeft} from 'assets';
import {TextSemiBold} from './Text';
import {colors} from 'utils';
import Gap from './Gap';

const Header = ({title, onPress, type}) => {
  switch (type) {
    case 'main':
      return (
        <View style={styles.containerMain}>
          <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
            <IcArrowLeft />
          </TouchableOpacity>
          <View style={styles.center}>
            <TextSemiBold text={title} type="Body" color={colors['Color-6']} />
          </View>
        </View>
      );
    default:
      return (
        <View style={styles.container}>
          <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
            <IcArrowLeft />
          </TouchableOpacity>
          <Gap width={22} />
          <TextSemiBold text={title} type="Body" color={colors['Color-6']} />
        </View>
      );
  }
};

export default Header;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors['Color-3'],
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 3,
  },
  containerMain: {
    backgroundColor: colors['Color-4'],
    paddingTop: 24,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  center: {
    flex: 1,
    alignItems: 'center',
    marginLeft: -40,
  },
});
