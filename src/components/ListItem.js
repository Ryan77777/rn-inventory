import {Dimensions, StyleSheet, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {PanGestureHandler} from 'react-native-gesture-handler';
import Animated, {
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import {IcTrash} from 'assets';
import {TextSemiBold, TextRegular} from './Text';
import {colors, NumberFormatter} from 'utils';
import Moment from 'moment';
import 'moment/locale/id';
import firestore from '@react-native-firebase/firestore';

const {width: SCREEN_WIDTH} = Dimensions.get('window');

const TRANSLATE_X_THRESHOLD = -SCREEN_WIDTH * 0.3;

const ListItem = ({data, onDelete, simultaneousHandlers, type}) => {
  const [product, setProduct] = useState({});
  const translateX = useSharedValue(0);
  const itemHeight = useSharedValue(65);
  const marginBottom = useSharedValue(16);
  const opacity = useSharedValue(1);

  const panGesture = useAnimatedGestureHandler({
    onActive: event => {
      translateX.value = event.translationX;
    },
    onEnd: () => {
      const shouldBeDismissed = translateX.value < TRANSLATE_X_THRESHOLD;
      if (shouldBeDismissed) {
        translateX.value = withTiming(-SCREEN_WIDTH);
        itemHeight.value = withTiming(0);
        marginBottom.value = withTiming(0);
        opacity.value = withTiming(0, undefined, isFinished => {
          if (isFinished && onDelete) {
            runOnJS(onDelete)(data);
          }
        });
      } else {
        translateX.value = withTiming(0);
      }
    },
  });

  const rStyle = useAnimatedStyle(() => ({
    transform: [
      {
        translateX: translateX.value,
      },
    ],
  }));

  const rIconStyle = useAnimatedStyle(() => {
    const opacityX = withTiming(
      translateX.value < TRANSLATE_X_THRESHOLD ? 1 : 0,
    );
    return {opacity: opacityX};
  });

  const rContainerStyle = useAnimatedStyle(() => {
    return {
      height: itemHeight.value,
      marginBottom: marginBottom.value,
      opacity: opacity.value,
    };
  });

  const getProduct = async id => {
    const x = await firestore().collection('Products').doc(id).get();
    setProduct(x.data());
  };

  useEffect(() => {
    getProduct(data.product);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return type === 'product' ? (
    <Animated.View style={[rContainerStyle]}>
      <PanGestureHandler
        simultaneousHandlers={simultaneousHandlers}
        onGestureEvent={panGesture}>
        <Animated.View style={[styles.card, rStyle]}>
          <View>
            <TextSemiBold
              text={data?.name}
              type="Paragraph"
              color={colors['Color-6']}
            />
            <TextRegular
              text={`Stok: ${data?.stock} item`}
              type="Smallest"
              color={colors['Color-5']}
            />
          </View>
          <TextSemiBold
            text={NumberFormatter(data?.price, 'Rp. ')}
            type="Paragraph"
            color={colors['Color-6']}
          />
        </Animated.View>
      </PanGestureHandler>
      <Animated.View style={[styles.iconContainer, rIconStyle]}>
        <IcTrash />
      </Animated.View>
    </Animated.View>
  ) : type === 'list' ? (
    <View style={styles.cardList}>
      <View>
        <TextSemiBold
          text={product?.name}
          type="Paragraph"
          color={colors['Color-6']}
        />
        <TextRegular
          text={`Jumlah: ${data?.total} item`}
          type="Smallest"
          color={colors['Color-5']}
        />
      </View>
      <TextSemiBold
        text={Moment(data?.date.toDate()).format('DD-MM-YYYY')}
        type="Paragraph"
        color={colors['Color-6']}
      />
    </View>
  ) : (
    <Animated.View style={[rContainerStyle]}>
      <PanGestureHandler
        simultaneousHandlers={simultaneousHandlers}
        onGestureEvent={panGesture}>
        <Animated.View style={[styles.card, rStyle]}>
          <View>
            <TextSemiBold
              text={product?.name}
              type="Paragraph"
              color={colors['Color-6']}
            />
            <TextRegular
              text={`Jumlah: ${data?.total} item`}
              type="Smallest"
              color={colors['Color-5']}
            />
          </View>
          <TextSemiBold
            text={Moment(data?.date.toDate()).format('DD-MM-YYYY')}
            type="Paragraph"
            color={colors['Color-6']}
          />
        </Animated.View>
      </PanGestureHandler>
      <Animated.View style={[styles.iconContainer, rIconStyle]}>
        <IcTrash />
      </Animated.View>
    </Animated.View>
  );
};

export default ListItem;

const styles = StyleSheet.create({
  card: {
    borderRadius: 8,
    padding: 12,
    borderWidth: 1,
    borderColor: colors['Color-5'],
    backgroundColor: colors['Color-3'],
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 24,
    zIndex: 1,
  },
  cardList: {
    borderRadius: 8,
    padding: 12,
    borderWidth: 1,
    borderColor: colors['Color-5'],
    backgroundColor: colors['Color-3'],
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    zIndex: 1,
    marginBottom: 16,
  },
  iconContainer: {
    height: 65,
    width: 65,
    position: 'absolute',
    right: '10%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
