import React from 'react';
import {StyleSheet, View} from 'react-native';
import Gap from './Gap';
import {TextRegular} from './Text';
import {colors} from 'utils';

const Divider = () => {
  return (
    <View style={styles.betweenWrapper}>
      <View style={styles.line} />
      <Gap width={8} />
      <TextRegular type="Body" text="Atau" color={colors['Color-5']} />
      <Gap width={8} />
      <View style={styles.line} />
    </View>
  );
};

export default Divider;

const styles = StyleSheet.create({
  betweenWrapper: {
    flexDirection: 'row',
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: colors['Color-5'],
    alignSelf: 'center',
  },
});
