import React from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import {colors} from 'utils';

const KeyboardAvoiding = ({children}) => {
  return (
    <KeyboardAvoidingView behavior="padding" style={styles.container}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        {children}
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

export default KeyboardAvoiding;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors['Color-3'],
  },
});
