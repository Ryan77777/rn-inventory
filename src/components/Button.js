import React from 'react';
import {ActivityIndicator, StyleSheet, TouchableOpacity} from 'react-native';
import {colors, scaleSize} from 'utils';
// import {Gap, TextBold} from 'components';
import Gap from './Gap';
import {TextBold} from './Text';

export const FilledButton = ({text, disable, isLoading, onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      style={styles.button(disable)}
      disabled={disable}
      onPress={onPress}>
      {isLoading && (
        <>
          <ActivityIndicator color={colors['Color-3']} />
          <Gap width={8} />
        </>
      )}
      <TextBold
        type="Body"
        text={text}
        color={disable ? colors['Color-4'] : colors['Color-3']}
      />
    </TouchableOpacity>
  );
};

export const TextButton = ({text, type = 'Paragraph', onPress}) => {
  return (
    <TouchableOpacity
      style={styles.textButton}
      activeOpacity={0.7}
      onPress={onPress}>
      <TextBold type={type} text={text} color={colors['Color-1']} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: disable => ({
    backgroundColor: disable ? colors['Color-9'] : colors['Color-1'],
    paddingVertical: scaleSize(10),
    paddingHorizontal: scaleSize(16),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scaleSize(8),
  }),
  textButton: {
    backgroundColor: 'transparent',
    paddingVertical: scaleSize(10),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scaleSize(8),
  },
});
