import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';
import {colors} from 'utils';

const Loading = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator color={colors['Color-1']} size="large" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: colors['Color-6'],
    opacity: 0.6,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
});

export default Loading;
