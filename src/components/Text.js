import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {scaleFont} from 'utils';

const getSize = type => {
  switch (type) {
    case 'Smallest':
      return 12;
    case 'Paragraph':
      return 14;
    case 'Body':
      return 16;
    case 'TextInput':
      return 18;
    case 'Title':
      return 20;
  }
};

const getLineHeight = size => {
  switch (size) {
    case 20:
      return 30;
    case 18:
      return 28;
    case 16:
      return 24;
    case 14:
      return 22;
    case 12:
      return 18;
  }
};

export const TextBold = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text style={[styles.bold(size, color, lineHeight), style]}>{text}</Text>
  );
};

export const TextSemiBold = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text style={[styles.semibold(size, color, lineHeight), style]}>
      {text}
    </Text>
  );
};

export const TextRegular = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text style={[styles.regular(size, color, lineHeight), style]}>{text}</Text>
  );
};

export const TextLight = ({style, type, text, color}) => {
  const size = getSize(type);
  const lineHeight = getLineHeight(size);
  return (
    <Text style={[styles.light(size, color, lineHeight), style]}>{text}</Text>
  );
};

const styles = StyleSheet.create({
  bold: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Bold',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
  semibold: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-SemiBold',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
  regular: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Regular',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
  light: (size, color, lineHeight) => ({
    fontFamily: 'Poppins-Light',
    fontSize: scaleFont(size),
    color: color,
    lineHeight: lineHeight,
  }),
});
