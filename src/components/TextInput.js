import React, {forwardRef, useEffect, useState} from 'react';
import {
  StyleSheet,
  TextInput as Input,
  TouchableOpacity,
  View,
} from 'react-native';
import {IcHide, IcSearch, IcShow} from '../assets';
import {colors, scaleFont, scaleSize} from '../utils';
import ErrorMessage from './ErrorMessage';
import Gap from './Gap';
import {TextRegular} from './Text';
import RNSearchablePicker from 'react-native-searchable-picker';

const TextInput = forwardRef((props, ref) => {
  const [isHidden, setIsHidden] = useState(true);
  const [borderWidth, setBorderWidth] = useState(1);
  const [borderColor, setBorderColor] = useState(colors['Color-5']);

  const onFocus = () => {
    setBorderWidth(2);
    setBorderColor(colors['Color-1']);
  };

  const onBlur = () => {
    setBorderWidth(1);
    setBorderColor(colors['Color-5']);
  };

  useEffect(() => {
    if (props.isError) {
      setBorderWidth(2);
      setBorderColor(colors['Color-2']);
    } else {
      setBorderWidth(1);
      setBorderColor(colors['Color-5']);
    }
  }, [props.isError]);

  switch (props.type) {
    case 'password':
      return (
        <>
          <View style={styles.forgetWrapper}>
            <View style={styles.row}>
              <TextRegular
                type="Paragraph"
                text={props.label}
                color={colors['Color-6']}
              />
              {props.isRequired && (
                <TextRegular
                  type="Paragraph"
                  text="*"
                  color={colors['Color-2']}
                />
              )}
            </View>
          </View>
          <Gap height={8} />
          <View style={styles.container(borderWidth, borderColor)}>
            <Input
              {...props}
              ref={ref}
              style={styles.input}
              placeholderTextColor={colors['Color-5']}
              secureTextEntry={isHidden}
              onFocus={onFocus}
              onBlur={onBlur}
            />
            <TouchableOpacity
              activeOpacity={0.7}
              onPress={() => setIsHidden(!isHidden)}>
              {isHidden ? <IcHide /> : <IcShow />}
            </TouchableOpacity>
          </View>
          <Gap height={4} />
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
          <Gap height={20} />
        </>
      );
    case 'search':
      return (
        <View style={styles.containerSearch}>
          <IcSearch />
          <Gap width={16} />
          <Input
            {...props}
            ref={ref}
            style={styles.input}
            placeholderTextColor={colors['Color-5']}
            onFocus={onFocus}
            onBlur={onBlur}
          />
        </View>
      );
    case 'disable':
      return (
        <>
          <View style={styles.row}>
            <TextRegular
              type="Paragraph"
              text={props.label}
              color={colors['Color-6']}
            />
            {props.isRequired && (
              <TextRegular
                type="Paragraph"
                text="*"
                color={colors['Color-2']}
              />
            )}
          </View>
          <Gap height={8} />
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.containerSearch}
            onPress={props.onPress}>
            <Input
              {...props}
              ref={ref}
              style={styles.input}
              placeholderTextColor={colors['Color-5']}
              editable={false}
            />
          </TouchableOpacity>
          <Gap height={4} />
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
          <Gap height={20} />
        </>
      );
    case 'select':
      return (
        <>
          <View style={styles.row}>
            <TextRegular
              type="Paragraph"
              text={props.label}
              color={colors['Color-6']}
            />
            {props.isRequired && (
              <TextRegular
                type="Paragraph"
                text="*"
                color={colors['Color-2']}
              />
            )}
          </View>
          <Gap height={8} />
          <TouchableOpacity activeOpacity={0.7} onPress={props.onPress}>
            <RNSearchablePicker
              data={props.data}
              placeholder="-- Pilih Barang --"
              onSelect={props.onSelect}
              containerStyles={styles.containerSelect}
              listStyles={styles.listStyles}
              itemStyles={styles.itemStyles}
              inputStyles={styles.inputStyles}
            />
          </TouchableOpacity>
          <Gap height={4} />
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
          <Gap height={20} />
        </>
      );
    default:
      return (
        <>
          <View style={styles.row}>
            <TextRegular
              type="Paragraph"
              text={props.label}
              color={colors['Color-6']}
            />
            {props.isRequired && (
              <TextRegular
                type="Paragraph"
                text="*"
                color={colors['Color-2']}
              />
            )}
          </View>
          <Gap height={8} />
          <View style={styles.container(borderWidth, borderColor)}>
            <Input
              {...props}
              ref={ref}
              style={styles.input}
              placeholderTextColor={colors['Color-5']}
              onFocus={onFocus}
              onBlur={onBlur}
            />
          </View>
          <Gap height={4} />
          <ErrorMessage text={props.errorMessage} isError={props.isError} />
          <Gap height={20} />
        </>
      );
  }
});

export default TextInput;

const styles = StyleSheet.create({
  container: (borderWidth, borderColor) => ({
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: scaleSize(42),
    paddingHorizontal: scaleSize(12),
    paddingVertical: scaleSize(10),
    borderWidth,
    borderColor,
    borderRadius: scaleSize(8),
  }),
  containerSearch: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    height: scaleSize(42),
    backgroundColor: colors['Color-4'],
    paddingHorizontal: scaleSize(12),
    paddingVertical: scaleSize(10),
    borderRadius: scaleSize(8),
  },
  containerSelect: {
    width: '100%',
    backgroundColor: colors['Color-4'],
    paddingHorizontal: scaleSize(12),
    borderRadius: scaleSize(8),
  },
  listStyles: {
    backgroundColor: colors['Color-3'],
    padding: 8,
    borderRadius: 8,
  },
  inputStyles: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: colors['Color-6'],
  },
  itemStyles: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    color: colors['Color-6'],
  },
  row: {
    flexDirection: 'row',
  },
  input: {
    flex: 1,
    height: scaleSize(42),
    fontFamily: 'Poppins-Regular',
    fontSize: scaleFont(14),
    color: colors['Color-6'],
    lineHeight: 22.4,
  },
  center: {
    width: '25%',
    alignItems: 'center',
  },
  textCenter: {textAlign: 'center'},
});
