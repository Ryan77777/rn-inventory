import React from 'react';
import Animated, {BounceInLeft, Transition} from 'react-native-reanimated';
import {colors} from 'utils';
import {TextLight} from './Text';

const ErrorMessage = ({text, isError}) =>
  isError ? (
    <Animated.View entering={BounceInLeft.duration(1000)} layout={Transition}>
      <TextLight type="Paragraph" text={text} color={colors['Color-2']} />
    </Animated.View>
  ) : null;

export default ErrorMessage;
