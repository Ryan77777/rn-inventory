import React from 'react';
import {StyleSheet} from 'react-native';
import Animated from 'react-native-reanimated';
import {colors} from 'utils';

const Container = ({
  children,
  backgroundColor = colors['Color-4'],
  center,
  entering,
  layout,
}) => {
  return (
    <Animated.View
      style={styles.container(center, backgroundColor)}
      entering={entering}
      layout={layout}>
      {children}
    </Animated.View>
  );
};

export default Container;

const styles = StyleSheet.create({
  container: (center, backgroundColor) => ({
    flex: 1,
    justifyContent: center && 'center',
    alignItems: center && 'center',
    backgroundColor: backgroundColor,
  }),
});
