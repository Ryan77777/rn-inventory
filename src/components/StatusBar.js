import React from 'react';
import {StatusBar as Status} from 'react-native';
import {colors} from 'utils';

const StatusBar = () => {
  return (
    <Status
      backgroundColor={colors['Color-1']}
      barStyle={'light-content'}
      translucent={false}
    />
  );
};

export default StatusBar;
