import {StyleSheet, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {colors, scaleSize} from 'utils';
import {TextSemiBold} from './Text';

const Card = ({title, icon, color = colors['Color-8'], onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.container}
      onPress={onPress}>
      <TextSemiBold type="Title" text={title} color={colors['Color-6']} />
      <View style={styles.rectangle(color)} />
      <View style={styles.icon}>{icon}</View>
    </TouchableOpacity>
  );
};

export default Card;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors['Color-3'],
    padding: scaleSize(24),
    height: 104,
    elevation: 3,
    borderRadius: scaleSize(16),
    overflow: 'hidden',
    marginBottom: 12,
  },
  rectangle: color => ({
    position: 'absolute',
    top: -20,
    right: -70,
    width: 150,
    height: 142,
    backgroundColor: color,
    borderRadius: scaleSize(24),
    transform: [{rotate: '24.7deg'}],
  }),
  icon: {
    position: 'absolute',
    top: 15,
    right: 40,
  },
});
