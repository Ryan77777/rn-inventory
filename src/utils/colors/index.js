export const colors = {
  'Color-1': '#18DCFF',
  'Color-2': '#FF3F34',
  'Color-3': '#FFFFFF',
  'Color-4': '#F5F6FA',
  'Color-5': '#A0A0A0',
  'Color-6': '#1E272E',
  'Color-7': '#0652DD',
  'Color-8': '#32FF7E',
  'Color-9': '#FCDDEC',
};
